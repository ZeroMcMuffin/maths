# maths
***

Maths is a REST API that accepts an array of numbers and returns mean, median, mode, and range.

## Features

1.  REST API that calculates mean, median, mode, and range.   

2.  That's it.  Don't act like you're not impressed.


## Requirements


1. [Composer](https://getcomposer.org/)

2. PHP 5.4+

3. Apache + mod_rewrite
 
## Installation

1.  Install the composer packages.  

2.  Configure a directory directive and document root to point the project's public directory.

3.  Ensure AllowOverride is set to All.

4.  Restart Apache.

## Usage

1.  Only application/json POSTS to /mmmr are currently accepted.

2.  POSTS should be in the following format:


```
#!JSON

{
   "numbers": [
       5, 6, 8, 7, 5 
   ]
}
```

Responses are structured as follows:


```
#!JSON
{
   "results": {
      "mean": 6.2,
      "median": 6,
      "mode": 5,
      "range": 3
    } 
}

```


## Tests

Unit tests exist for the Maths library.  To run the tests, do the following:


```
#!bash

phpunit --bootstrap lib/*  tests/
```
*Note:  Tests require PHPUnit 3.7, which is included in the dev composer requirements.

### Test Endpoint
I maintain a test endpoint here:  http://107.170.213.148/mmmr