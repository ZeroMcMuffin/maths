<?php

class MathsTest extends PHPUnit_Framework_TestCase
{
    protected $evenArray;
    protected $oddArray;
    protected $invalidArray;
    protected $singleModeArray;
    protected $doubleModeArray;

    protected function setUp()
    {
        $this->evenArray = [1, 3, 4, 2];
        $this->oddArray = [2, 4, 5, 3, 1];
        $this->invalidArray = [1, "b", 3];
        $this->singleModeArray = [1, 3, 3, 5];
        $this->doubleModeArray = [1, 2, 2, 3, 3, 4];
    }

    public function testApiExists()
    {
        $this->assertTrue(method_exists('Maths', 'getMean'));
        $this->assertTrue(method_exists('Maths', 'getMedian'));
        $this->assertTrue(method_exists('Maths', 'getRange'));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArraySize()
    {
        $invalidLength = [1];
        Maths::getMean($invalidLength);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testNonNumericValue()
    {
        Maths::getMean($this->invalidArray);
    }


    public function testGetMean()
    {
        $this->assertEquals(Maths::getMean($this->evenArray), 2.5);
    }

    public function testGetMedian()
    {
        $this->assertEquals(Maths::getMedian($this->evenArray), 2.5);
        $this->assertEquals(Maths::getMedian($this->oddArray), 3);
    }


    public function testGetMode()
    {
        $rangeSingle = Maths::getMode($this->singleModeArray);
        $this->assertTrue(count($rangeSingle) === 1);
        $this->assertEquals($rangeSingle, 3);

        $rangeDouble = Maths::getMode($this->doubleModeArray);
        $this->assertTrue(count($rangeDouble) === 2);
        $this->assertEquals($rangeDouble[0], 3);
        $this->assertEquals($rangeDouble[1], 2);

        $rangeNull = Maths::getMode($this->evenArray);
        $this->assertNull($rangeNull);
    }

    public function testGetRange()
    {
        $range = Maths::getRange($this->oddArray);
        $this->assertEquals($range[0], 1);
        $this->assertEquals($range[1], 5);
    }
}