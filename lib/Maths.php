<?php

/**
 * Class Maths
 * Utility functions for mean, median, mode, and range
 * @author Allen Elks
 */
class Maths
{
    public static function getMean(array $numbers, $precision = 3)
    {
        self::validateInput($numbers);
        return number_format(array_sum($numbers) / count($numbers), $precision);
    }

    public static function getMedian(array $numbers)
    {
        self::validateInput($numbers);
        sort($numbers);
        $arraySize = count($numbers);
        if ($arraySize % 2 === 0) { // even number of elements requires mean of 2 elements
            $element1 = $numbers[intval($arraySize / 2)];
            $element2 = $numbers[intval($arraySize / 2 - 1)];
            return self::getMean([$element1, $element2]);
        } else { // odd number of elements
            return $numbers[intval($arraySize / 2)];
        }
    }

    public static function getMode(array $numbers)
    {
        self::validateInput($numbers);
        $values = array_count_values($numbers);
        arsort($values);
        $mode = array_keys($values, max($values), true);

        if (count($mode) === 1) { // single modal
            return $mode[0];
        } else if (count($mode) === count($numbers)) { // no mode
            return null;
        } else { // multi modal
            return $mode;
        }
    }

    public static function getRange($numbers)
    {
        self::validateInput($numbers);
        sort($numbers);
        return ([reset($numbers), end($numbers)]);
    }

    /**
     * Ensure that we have a proper data set to work with.
     * @param $numbers
     * @throws InvalidArgumentException
     */
    private static function validateInput(array $numbers)
    {
        if (count($numbers) < 2) {
            throw new InvalidArgumentException("You must supply at least two numbers");
        }
        foreach ($numbers as $key => $number) { // determine if each element is numeric
            if (!is_numeric($number)) {
                throw new InvalidArgumentException("Array contained non-numeric values ");
            }
        }
    }
}


