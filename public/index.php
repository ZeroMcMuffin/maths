<?php
/**
 * Slim framework application.
 *
 * My current feeling is that misdirection, abstraction, etc are not warranted to
 * solve this issue.  If this project were to grow, and there are no current plans for this,
 * I'd opt for project structure and OOP practices.
 * @author: Allen Elks
 */
require '../vendor/autoload.php';
require '../lib/Maths.php';
use Slim\Slim;

$app = new Slim();
$app->contentType('application/json;charset=utf-8');
$app->config('debug', false);

// Routing
$app->post('/mmmr', 'getMaths');
$app->get('/mmmr', 'returnInvalidRequest');
$app->put('/mmmr', 'returnInvalidRequest');
$app->delete('/mmmr', 'returnInvalidRequest');
$app->post('/*', 'returnInvalidRequest');
$app->get('/*', 'returnInvalidRequest');
$app->put('/*', 'returnInvalidRequest');
$app->delete('/*', 'returnInvalidRequest');

// Catch-all for general application errors
$app->error(function(Exception $e) use ($app) {
    $app->status(500);
    $response = $app->response();
    $responseData = array('error' => array('code' => 500, 'message' => 'Oops--we zigged when we should have zagged.'));
    $response->body(json_encode($responseData));
});
$app->run();

// Does all the work
function getMaths()
{
    $app = Slim::getInstance();
    $params = json_decode($app->request()->getBody());
    $numbers = $params->numbers;
    $mean = Maths::getMean($numbers);
    $median = Maths::getMedian($numbers);
    $mode = Maths::getMode($numbers);
    $range = Maths::getRange($numbers);
    $responseData = array('results' => array(
        'mean' => $mean,
        'median' => $median,
        'mode' => $mode,
        'range' =>$range));
    $response = $app->response();
    $response->body(json_encode($responseData));
}

// Handle requests not supported by our 'robust' API
function returnInvalidRequest()
{
    $app = Slim::getInstance();
    $app->status(404);
    $responseData = array('error' => array('code' => 404, 'message' => 'Method  ' .
        $app->request()->getMethod() . ' for ' . $app->request()->getPath() . ' is not supported.'));
    $response = $app->response();
    $response->body(json_encode($responseData));
}


